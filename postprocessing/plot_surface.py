import matplotlib.animation as animation
import matplotlib.pyplot as plt
import numpy as np
import vtk
from vtkmodules.util.numpy_support import vtk_to_numpy

plt.style.use("stylesheet.mpl")


def read_vtp(path):
    reader = vtk.vtkXMLPolyDataReader()
    reader.SetFileName(path)
    reader.Update()
    data = reader.GetOutput().GetPointData()
    field_count = data.GetNumberOfArrays()
    return {
        data.GetArrayName(i): vtk_to_numpy(data.GetArray(i)) for i in range(field_count)
    }


def load_surface(params):
    steps = int(params.N / params.output_period)
    height = np.zeros((steps + 1, params.Mx + 1))
    velocity = np.copy(height)
    for i in range(steps + 1):
        output = read_vtp(params.output_directory / f"surface_{i:05}.vtp")
        height[i] = output.get("height", np.full(params.Mx + 1, np.nan))
        velocity[i] = output.get("velocity", np.full(params.Mx + 1, np.nan))
    return height, velocity


def animate_surface(h: np.ndarray, v: np.ndarray, params, **kwargs):
    fig, ax = plt.subplots()

    x = np.linspace(0, params.Lx.value, params.Mx + 1)
    h_line = ax.plot(x, h[0], color="k", label="height")[0]
    v_line = ax.plot(x, v[0], color="k", ls="--", label="velocity")[0]

    ax.set(**kwargs)
    ax.set(
        title=f"Surface at $t={0.0}s$",
        xlabel="z",
        ylabel=r"Height, Velocity",
        ylim=[0.0, 1.1 * max(h.max(), v.max())],
    )
    ax.legend()

    def update(frame):
        n = frame
        h_line.set_ydata(h[n])
        v_line.set_ydata(v[n])
        ax.set(title=rf"Surface at $t={n*params.output_period*params.dt}s$")

    ani = animation.FuncAnimation(fig=fig, func=update, frames=h.shape[0])
    return ani

import warnings

import matplotlib.animation as animation
import matplotlib.pyplot as plt
import meshio
import numpy as np

from coupling.params import Params, load_params

plt.style.use("stylesheet.mpl")


def plot_psi(n: int, params: Params):
    mesh = meshio.read(params.output_directory / f"richards_{n:05}.vtu")
    psi = mesh.point_data["psi"]
    x = np.linspace(0, params.Lx.value, params.Mx + 1)
    z = np.linspace(0, params.Lz.value, params.Mz + 1)
    X, Z = np.meshgrid(x, z)
    psi = np.reshape(psi, (params.Mz + 1, params.Mx + 1))
    fig, ax = plt.subplots()
    fig.set(figwidth=10, figheight=6)
    im = ax.pcolormesh(X, Z, psi, vmin=psi.min(), vmax=psi.max())
    ax.set(
        xlabel="x",
        ylabel=r"z",
        title=rf"$\psi(x,z)$ at $t={n*params.output_period*params.dt}$",
    )
    fig.colorbar(im)
    plt.show()
    return fig


def animate(arr: np.ndarray, params: Params, **kwargs):
    fig, ax = plt.subplots()
    fig.set(figwidth=10, figheight=6)
    arr_min, arr_max = arr.min(), arr.max()

    x = np.linspace(0, params.Lx.value, params.Mx + 1)
    z = np.linspace(0, params.Lz.value, params.Mz + 1)
    X, Z = np.meshgrid(x, z)
    im = ax.pcolormesh(
        X,
        Z,
        arr[0],
        vmin=arr_min,
        vmax=arr_max,
    )
    fig.colorbar(im)

    name = kwargs.pop("name", r"\psi")

    ax.set(**kwargs)
    ax.set(
        title=rf"${name}$ at $t={0.0}s$",
        xlabel="z",
        ylabel=rf"${name}(t,z)$",
    )

    def update(frame):
        n = frame
        ax.pcolormesh(
            X,
            Z,
            arr[n],
            vmin=arr_min,
            vmax=arr_max,
        )
        ax.set(title=rf"${name}$ at $t={n*params.output_period*params.dt}s$")

    ani = animation.FuncAnimation(fig=fig, func=update, frames=arr.shape[0])
    return ani


def animate_line(arr: np.ndarray, params: Params, **kwargs):
    fig, ax = plt.subplots()

    z = np.linspace(0, params.Lz.value, params.Mz + 1)
    x_index = int((params.Mx + 1) / 2)
    line = ax.plot(z, arr[0, :, x_index], color="k")[0]

    name = kwargs.pop("name", r"\psi")

    ax.set(**kwargs)
    ax.set(
        title=rf"${name}$ at $t={0.0}s$",
        xlabel="z",
        ylabel=rf"${name}(t,z)$",
        ylim=[arr.min(), arr.max()],
    )

    def update(frame):
        n = frame
        line.set_ydata(arr[n, :, 3])
        ax.set(title=rf"${name}$ at $t={n*params.output_period*params.dt}s$")

    ani = animation.FuncAnimation(fig=fig, func=update, frames=arr.shape[0])
    return ani


def line_plot(n: int, params: Params, name_in_file: str, name_for_plot: str):
    mesh = meshio.read(params.output_directory / f"richards_{n:05}.vtu")
    arr = mesh.point_data[name_in_file]
    arr = np.reshape(arr, (params.Mz + 1, params.Mx + 1))
    arr = arr[:, int(arr.shape[1] / 2)]
    fig, ax = plt.subplots()
    z = np.linspace(0, params.Lz.value, params.Mz + 1)
    ax.plot(z, arr, color="k")
    ax.set(
        xlabel="z",
        ylabel=rf"${name_for_plot}(t,z)$",
        title=rf"${name_for_plot}$ at $t={n*params.output_period*params.dt}$",
    )
    plt.show()
    return fig


def line_plot_psi(n: int, params: Params):
    return line_plot(n, params, "psi", r"\psi")


def line_plot_capacity(n: int, params: Params):
    return line_plot(n, params, "capacity", "c")


def line_plot_conductivity(n: int, params: Params):
    return line_plot(n, params, "conductivity", "K")


def plot_product(n: int):
    mesh = meshio.read(params.output_directory / f"richards_{n:05}.vtu")
    arr = mesh.point_data["conductivity"] * mesh.point_data["capacity"]
    arr = np.reshape(arr, (params.Mz + 1, params.Mx + 1))
    arr = arr[:, int(arr.shape[1] / 2)]
    fig, ax = plt.subplots()
    ax.plot(z, arr, color="k")
    ax.set(
        xlabel="z",
        ylabel=r"$c\cdot K$",
        title=rf"$c\cdot K$ at $t={n*params.output_period*params.dt}$",
    )
    plt.show()
    return fig


def load_data(params):
    steps = int(params.N / params.output_period)
    psi = np.zeros((steps + 1, params.Mz + 1, params.Mx + 1))
    capacity = np.zeros_like(psi)
    conductivity = np.zeros_like(psi)
    new_shape = (params.Mz + 1, params.Mx + 1)
    for n in range(steps + 1):
        try:
            mesh = meshio.read(params.output_directory / f"richards_{n:05}.vtu")
        except meshio.ReadError:
            continue
        psi_n = mesh.point_data["psi"]
        capacity_n = mesh.point_data["capacity"]
        conductivity_n = mesh.point_data["conductivity"]

        psi[n] = np.reshape(psi_n, new_shape)
        capacity[n] = np.reshape(capacity_n, new_shape)
        conductivity[n] = np.reshape(conductivity_n, new_shape)

    return psi, capacity, conductivity


if __name__ == "__main__":
    params = load_params("params.yaml")
    z = np.linspace(0, params.Lz.value, params.Mz + 1)

    psi, capacity, conductivity, product = load_data()

    ani = animate(psi, ylim=[1.1 * psi.min(), 1.1 * psi.max()])
    with warnings.catch_warnings(action="ignore"):
        ani.save(filename="richards_psi.mp4", writer="ffmpeg")
    plt.close("all")

    ani = animate(capacity, name="c")
    with warnings.catch_warnings(action="ignore"):
        ani.save(filename="richards_c.mp4", writer="ffmpeg")
    plt.close("all")

    ani = animate(conductivity, name="K")
    with warnings.catch_warnings(action="ignore"):
        ani.save(filename="richards_K.mp4", writer="ffmpeg")
    plt.close("all")

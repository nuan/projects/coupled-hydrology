from dataclasses import dataclass

import ufl


def interpolate(left_value, right_value, x, L, slope):
    tanh_value = 0.5 * (ufl.tanh(slope * (x - L / 2)) + 1)
    return (1 - tanh_value) * left_value + tanh_value * right_value


@dataclass
class Soil:
    theta_S: float
    theta_R: float
    alpha: float
    nVG: float
    Ks: float


class MixedSoil:
    """Class for homogeneous and horizontally varying, heterogeneous soil."""

    def __init__(
        self,
        left_soil: Soil,
        right_soil: Soil = None,
        length: float = None,
        slope: float = None,
    ) -> None:
        self.soil1 = left_soil
        self.soil2 = right_soil
        self.length = length
        self.slope = slope

    def interpolate_property(self, prop: str, x: float) -> float:
        property1 = getattr(self.soil1, prop)
        if self.soil2 is None:
            return property1
        property2 = getattr(self.soil2, prop)
        return interpolate(property1, property2, x, self.length, self.slope)

    def alpha(self, x: float) -> float:
        return self.interpolate_property("alpha", x)

    def theta_S(self, x: float) -> float:
        return self.interpolate_property("theta_S", x)

    def theta_R(self, x: float) -> float:
        return self.interpolate_property("theta_R", x)

    def Ks(self, x: float) -> float:
        return self.interpolate_property("Ks", x)

    def nVG(self, x: float) -> float:
        return self.interpolate_property("nVG", x)


silt_loam = Soil(
    theta_S=0.396,
    theta_R=0.131,
    alpha=0.423,
    nVG=2.06,
    Ks=4.96 * 10 ** (-2) / 86400,
)


beit_netofa_clay = Soil(
    theta_S=0.446,
    theta_R=0.0,
    alpha=0.152,
    nVG=1.17,
    Ks=8.2 * 10 ** (-4) / 86400,
)

sandy_loam_4 = Soil(
    Ks=6.94e-4 / 60,
    nVG=2.0,
    alpha=1.0 * 100,
    theta_R=0.2,
    theta_S=1.0,
)

sandy_loam_5 = Soil(
    Ks=6.94e-5 / 60,
    nVG=2.0,
    alpha=1.0 * 100,
    theta_R=0.2,
    theta_S=1.0,
)

sandy_loam_6 = Soil(
    Ks=6.94e-6 / 60,
    nVG=2.0,
    alpha=1.0 * 100,
    theta_R=0.2,
    theta_S=1.0,
)

# from Fayer et al. (1992)
sand = Soil(theta_S=0.445, theta_R=0.010, alpha=7.26, nVG=2.80, Ks=1.094e-3)
gravel = Soil(theta_S=0.419, theta_R=0.005, alpha=493, nVG=2.19, Ks=3.5e-3)


def create_soil(soil_type: str, Lx: float = None, slope: float = None) -> MixedSoil:
    if soil_type == "mixed":
        return MixedSoil(silt_loam, beit_netofa_clay, Lx, slope)
    if soil_type == "loam":
        return MixedSoil(silt_loam)
    if soil_type == "clay":
        return MixedSoil(beit_netofa_clay)
    if soil_type == "gravel":
        return MixedSoil(gravel)
    if soil_type == "sand":
        return MixedSoil(sand)
    if soil_type == "sandy_loam_4":
        return MixedSoil(sandy_loam_4)
    if soil_type == "sandy_loam_5":
        return MixedSoil(sandy_loam_5)
    if soil_type == "sandy_loam_6":
        return MixedSoil(sandy_loam_6)
    raise ValueError(f"Soil type {soil_type} not supported.")

from dataclasses import dataclass, field
from pathlib import Path

import jinja2
import numpy as np
from dune.ufl import Constant
from ruamel.yaml import YAML

from coupling.soil import MixedSoil, create_soil


@dataclass
class Params:
    t_0: float
    t_end: float
    N: int
    Mx: int
    Lx: Constant | float
    dt: float = field(init=False)
    soil: MixedSoil = field(init=False)
    output_directory: str | Path
    output_period: int
    positions_gnd: list = field(init=False)
    positions_sfc: list = field(init=False)
    surface_slope: float = 0.0
    mannings_coefficient: float = 0.0
    tolerance: float = 0.0
    max_iterations: int = 0
    min_iterations: int = 0
    coupling_scheme: str = ""
    omega: float = 0.0
    soil_type: str = "loam"
    precice_config: str | Path = ""
    precice_config_template: str | Path = ""
    Lz: Constant | float = 0.0
    Mz: int = 0

    def __post_init__(self):
        self.dt = (self.t_end - self.t_0) / self.N
        self.Lx = Constant(self.Lx, "Lx")
        self.Lz = Constant(self.Lz, "Lz")
        self.precice_config = Path(self.precice_config)
        self.precice_config_template = Path(self.precice_config_template)
        self.output_directory = Path(self.output_directory)
        if self.output_period != 0:
            self.output_directory.mkdir(exist_ok=True)
        self.soil = create_soil(self.soil_type, self.Lx, 4.0)
        assert self.output_period >= 0
        assert self.precice_config_template.exists()
        self.positions_gnd = compute_ground_positions(self.Lx, self.Mx)
        self.positions_sfc = compute_surface_positions(self.Lx, self.Mx)


def compute_ground_positions(Lx: Constant, Mx: int):
    nodes = np.linspace(0, Lx.value, Mx + 1)
    positions = [[0, x] for x in nodes]
    return positions


def compute_surface_positions(Lx: Constant, Mx: int) -> list:
    dx = Lx.value / Mx
    cells = np.linspace(dx / 2, Lx.value - dx / 2, Mx)
    positions = [[0, x] for x in cells]
    return positions


def get_template(template_path: Path) -> jinja2.Template:
    """get Jinja2 template file"""
    loader = jinja2.FileSystemLoader(template_path.parent)
    environment = jinja2.Environment(loader=loader, undefined=jinja2.StrictUndefined)
    return environment.get_template(template_path.name)


def render(params: Params) -> None:
    jinja_template = get_template(params.precice_config_template)
    with open(params.precice_config, "w") as precice_config:
        precice_config.write(
            jinja_template.render(
                coupling_scheme=params.coupling_scheme,
                N=params.N,
                dt=params.dt,
                tolerance=params.tolerance,
                max_iterations=params.max_iterations,
                omega=params.omega,
                min_iterations=params.min_iterations,
            )
        )


def load_params(yaml_file: Path | str) -> Params:
    yaml = YAML(typ="safe")
    if isinstance(yaml_file, str):
        yaml_file = Path(yaml_file)
    params = yaml.load(yaml_file)
    params = Params(**params)
    return params


def write_output(step: int, period: int, output_writer: callable) -> None:
    if period == 0:
        return
    if step % period == 0:
        output_writer()

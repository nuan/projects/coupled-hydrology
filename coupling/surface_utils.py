from dune.fem.operator import molGalerkin


class MolOperator:
    """Method of lines Galerkin operator that can be treated by DUNE time steppers."""

    def __init__(self, form, space, dt: float):
        # molGalerkin automatically includes the inverse mass matrix:
        self._op = molGalerkin(form)
        self.space = space
        self.localTimeStepEstimate = [dt]

    def __call__(self, u, v):
        self._op(u, v)

    def stepTime(self, t0, dt0):
        pass

    def applyLimiter(self, u):
        pass

import math

import numpy as np
import ufl
from dune.fem.function import gridFunction
from dune.fem.space import lagrange
from dune.fem.utility import lineSample
from dune.ufl import DirichletBC, GridFunction

from coupling.params import Params


class GroundCouplingLayer:
    """Coupling layer to provide to Ground when running coupled surface-subsurface flow."""

    def __init__(self, params: Params) -> None:
        self.river_height = np.zeros(params.Mx)
        self.Lx = params.Lx
        self.Lz = params.Lz
        self.dx = params.Lx / params.Mx
        self.Mx = params.Mx

    def mapped_interface_flux(self, vertical_flux: GridFunction):
        _, interface_flux = lineSample(
            vertical_flux,
            [self.dx / 2, self.Lz],
            [self.Lx - self.dx / 2, self.Lz],
            self.Mx,
        )

        return interface_flux

    def get_boundary_condition(self, grid, space):
        @gridFunction(grid, name="h", order=1)
        def h(position):
            i = max(0, math.ceil(position[0] / self.dx) - 1)
            return self.river_height[i]

        z = ufl.SpatialCoordinate(space)[1]

        bc1 = DirichletBC(
            space,
            h,
            ufl.conditional(z > (self.Lz.value - 1e-8), 1, 0),
        )
        return bc1


class KinWaveCouplingLayer:
    def __init__(self, params: Params) -> None:
        self._source_term = np.zeros(params.Mx)

    def mapped_river_height(self, surface_state) -> np.ndarray:
        grid = surface_state.gridView
        fem_space = lagrange(grid, order=1)
        fem_state = fem_space.function(name="fem_state")
        fem_state.project(surface_state)
        return fem_state.as_numpy

    @property
    def source_term(self) -> np.ndarray:
        return self._source_term

    @source_term.setter
    def source_term(self, river_height: np.ndarray) -> None:
        self._source_term[:] = river_height


class ShallowWaterCouplingLayer:
    def __init__(self, params: Params) -> None:
        self._source_term = np.zeros(2 * params.Mx)

    def mapped_river_height(self, surface_state) -> np.ndarray:
        grid = surface_state.gridView
        fem_space = lagrange(grid, order=1, dimRange=2)
        fem_state = fem_space.function(name="fem_state")
        fem_state.project(surface_state)
        return fem_state.as_numpy[::2]

    @property
    def source_term(self) -> np.ndarray:
        return self._source_term

    @source_term.setter
    def source_term(self, river_height: np.ndarray) -> None:
        self._source_term[::2] = river_height

from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np

from linear_model.analysis import compute_coupling_behavior
from linear_model.process_results import compute_convergence_rate
from linear_model.setup_simulation import Params
from linear_model.simulation import run_coupled_simulation

params = Params(
    tolerance=1e-8,
    max_iterations=15,
    min_iterations=2,
    omega=1,
    coupling_scheme="serial-implicit",
    K=1.0,
    c=1.0,
    L=1.0,
    h_0=1.0,
    t_0=0,
    t_end=0.1,
    N=1,
    M=20,
    bc_type="dirichlet",
    dirichlet_value=0.0,
    ic_type="linear",
    precice_config_template="linear_model/precice-config.xml.j2",
    precice_config="linear_model/precice-config.xml",
)

if __name__ == "__main__":
    plt.style.use("stylesheet.mpl")
    plotting_dir = Path("plots/analysis_verification")
    # plotting_dir = Path(
    #     "/home/valentina/Nextcloud/PhD/Richards-SWE/richards-swe-paper/plots/"
    # )
    plotting_dir.mkdir(parents=True, exist_ok=True)
    omegas = np.array(
        [1e-3, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.73, 0.75, 0.76, 0.78, 0.8, 0.9, 1.0]
    )
    omegas = np.linspace(0.001, 1, 21)
    convergence_rates = np.zeros(omegas.shape)
    sigma_analysis = np.zeros(omegas.shape)

    _, S, _ = compute_coupling_behavior(
        params.K, params.c, params.L, params.dt, params.M
    )
    sigma_analysis = omegas * S + (1 - omegas)

    for index, omega in enumerate(omegas):
        params.omega = omega
        run_coupled_simulation(params)
        convergence_rates[index] = compute_convergence_rate(
            "precice-RiverSolver-convergence.log"
        )

    fig, ax = plt.subplots(layout="constrained")
    fig.set(figwidth=3, figheight=2.5)
    ax.plot(
        omegas,
        convergence_rates,
        color="k",
        marker="x",
        ls="none",
        label=r"observed CR",
    )
    ax.plot(
        omegas,
        abs(sigma_analysis),
        marker="o",
        mfc="none",
        ls="none",
        color="k",
        label=r"$|\Sigma(\omega)|$",
    )
    ax.set(
        xlabel=r"$\omega$",
        ylabel="Convergence Factor",
        xlim=[0, 1],
        ylim=[0, 1],
    )
    ax.legend(ncols=1, edgecolor="k")
    fig.savefig(plotting_dir / "convergence_rates.pdf")

from pathlib import Path

import numpy as np
import xarray as xr

from linear_model.analysis import compute_coupling_behavior
from linear_model.process_results import compute_convergence_rate
from linear_model.setup_simulation import Params
from linear_model.simulation import run_coupled_simulation

params = Params(
    tolerance=1e-8,
    max_iterations=20,
    omega=1,
    coupling_scheme="serial-implicit",
    min_iterations=2,
    K=1.0,
    c=1.0,
    L=1.0,
    h_0=1.0,
    t_0=0,
    t_end=1,
    N=1,
    M=20,
    bc_type="dirichlet",
    dirichlet_value=0.0,
    ic_type="linear",
    precice_config_template="linear_model/precice-config.xml.j2",
    precice_config="linear_model/precice-config.xml",
)

if __name__ == "__main__":
    plotting_dir = Path("plots/analysis_verification")
    plotting_dir.mkdir(parents=True, exist_ok=True)

    t_values = np.power(10.0, np.arange(-8, 1))
    S_analysis = np.zeros(t_values.shape)
    convergence_rates = np.zeros(t_values.shape)

    for index, t in enumerate(t_values):
        params.dt = t
        params.t_end = t
        S_analysis[index] = compute_coupling_behavior(
            params.K, params.c, params.L, params.dt, params.M
        )[1]

        run_coupled_simulation(params)
        convergence_rates[index] = compute_convergence_rate(
            "precice-RiverSolver-convergence.log"
        )

    ds = xr.Dataset(
        {
            r"|S|, $\Delta z=1/20$": abs(S_analysis),
            r"CR, $\Delta z=1/20$": convergence_rates,
        },
        coords={r"$\Delta t$": t_values},
    )

    S_analysis = np.zeros(t_values.shape)
    convergence_rates = np.zeros(t_values.shape)
    params.M = 500
    params.dz = params.L / 500

    for index, t in enumerate(t_values):
        params.dt = t
        params.t_end = t
        S_analysis[index] = compute_coupling_behavior(
            params.K, params.c, params.L, params.dt, params.M
        )[1]

        run_coupled_simulation(params)
        convergence_rates[index] = compute_convergence_rate(
            "precice-RiverSolver-convergence.log"
        )

    ds[r"|S|, $\Delta z=1/500$"] = abs(S_analysis)
    ds[r"CR, $\Delta z=1/500$"] = convergence_rates

    ds.to_netcdf(plotting_dir / "cvg_no_relaxation.nc")

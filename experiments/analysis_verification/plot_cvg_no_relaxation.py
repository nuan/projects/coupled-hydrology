from pathlib import Path

import matplotlib.pyplot as plt
import xarray as xr

plt.style.use("stylesheet.mpl")
plotting_dir = Path("plots/analysis_verification")
plotting_dir.mkdir(parents=True, exist_ok=True)

ds = xr.load_dataset(plotting_dir / "cvg_no_relaxation.nc")
# plotting_dir = Path(
#     "/home/valentina/Nextcloud/PhD/Richards-SWE/richards-swe-paper/plots/"
# )

fig, ax = plt.subplots(layout="constrained")
fig.set(figwidth=3, figheight=2.5)
ax = ax
ax.plot(
    ds[r"$\Delta t$"],
    ds[r"CR, $\Delta z=1/20$"],
    color="k",
    marker="x",
    ls="none",
    label="observed CR",
)
ax.plot(
    ds[r"$\Delta t$"],
    ds[r"|S|, $\Delta z=1/20$"],
    marker="o",
    mfc="none",
    ls="none",
    color="k",
    label="$|S|$",
)
ax.set(
    yscale="log",
    xscale="log",
    xlabel=r"$\Delta t$",
    ylabel="Convergence Factor",
    ylim=[1e-4, 2e0],
)
ax.legend(edgecolor="k")
fig.savefig(plotting_dir / "convergence_no_relaxation_1_20.pdf", bbox_inches="tight")

fig, ax = plt.subplots(layout="constrained")
fig.set(figwidth=3, figheight=2.5)
ax.plot(
    ds[r"$\Delta t$"],
    ds[r"CR, $\Delta z=1/500$"],
    color="k",
    marker="x",
    ls="none",
    label="observed CR",
)
ax.plot(
    ds[r"$\Delta t$"],
    ds[r"|S|, $\Delta z=1/500$"],
    marker="o",
    mfc="none",
    ls="none",
    color="k",
    label="$|S|$",
)
ax.set(
    yscale="log",
    xscale="log",
    xlabel=r"$\Delta t$",
    ylabel="Convergence Factor",
    ylim=[1e-4, 2e0],
)
ax.legend(edgecolor="k")

fig.savefig(plotting_dir / "convergence_no_relaxation_1_500.pdf", bbox_inches="tight")

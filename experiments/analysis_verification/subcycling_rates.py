from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np

from linear_model.analysis import compute_coupling_behavior, get_sigma

plotting_dir = Path("plots/analysis_verification")
plotting_dir.mkdir(parents=True, exist_ok=True)
plt.style.use("stylesheet.mpl")

K = 1.0
c = 1.0
L = 1
dt = 0.1
M = 10

_, S, omega = compute_coupling_behavior(K, c, L, dt, M)

fig, axs = plt.subplots(nrows=1, ncols=2, sharex=False, layout="constrained")
fig.set(figwidth=8, figheight=4)

ax = axs[0]
omegas = np.linspace(0, 1, 300)

sc_steps = np.array([1, 2, 4, 8])
markers = ["x", ".", "+", "1"]
linestyles = ["-", "--", ":", "-."]

for index, m in enumerate(sc_steps):
    ax.plot(
        omegas,
        abs(get_sigma(omegas, (m + 1) / (2 * m) * S)),
        # marker=markers[index],
        ls=linestyles[index],
        label=f"{m=}",
    )
ax.legend()
ax.set(
    xlabel=r"$\omega$",
    title=r"Convergence rate for varying $\omega$",
)


m = np.arange(1, 17)
S_sc = (m + 1) / (2 * m) * S
assert S_sc[0] == S

cvg_rates = abs(get_sigma(omega, S_sc))

ax = axs[1]
ax.scatter(m, cvg_rates, color="k", marker=".")
ax.set(
    xlabel="m",
    title=r"Convergence rate for $\omega=\omega_\mathrm{opt}$",
)

fig.supylabel("Convergence Rate")
fig.savefig(plotting_dir / "subcycling_rates.pdf")

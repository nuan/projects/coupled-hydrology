import matplotlib.pyplot as plt
import numpy as np

from coupling.soil import Soil, beit_netofa_clay, gravel, sand, silt_loam


def theta_rel(psi: float, soil: Soil):
    if psi > 0:
        return 1
    nVG = soil.nVG
    m = 1 - 1 / nVG
    Se = (1 + (-soil.alpha * psi) ** nVG) ** (-m)
    return Se


def psi(theta_rel: float, soil: Soil):
    if theta_rel <= 0 or theta_rel > 1:
        raise ValueError("Relative saturation must be in (0, 1].")
    n = soil.nVG
    m = 1 - 1 / n
    inner_root = theta_rel ** (-1 / m)
    outer_root = (inner_root - 1) ** (n)
    return (-1 / soil.alpha) * outer_root


def theta_abs_from_rel(theta_rel: float, soil: Soil):
    if theta_rel < 0 or theta_rel > 1:
        raise ValueError("Relative saturation must be in [0, 1].")
    return soil.theta_R + theta_rel * (soil.theta_S - soil.theta_R)


def theta_rel_from_abs(theta_abs: float, soil: Soil):
    if theta_abs > soil.theta_S:
        raise ValueError("theta > theta_S")
    if theta_abs < soil.theta_R:
        raise ValueError("theta <= theta_R")
    return (theta_abs - soil.theta_R) / (soil.theta_S - soil.theta_R)


if __name__ == "__main__":
    plt.style.use("stylesheet.mpl")

    trel_values = np.linspace(0.5, 1, 50)
    fig, ax = plt.subplots()
    psiv = np.vectorize(psi)
    names = iter(["loam", "clay", "gravel", "sand"])

    for soil in [silt_loam, beit_netofa_clay, gravel, sand]:
        ax.semilogy(trel_values, -psiv(trel_values, soil), label=next(names))
    ax.legend()
    ax.set(
        xlabel=r"$\theta_\mathrm{rel}$",
        ylabel=r"$-\psi$",
        title=r"$\psi(\theta_\mathrm{rel})$ for Different Soils",
    )
    fig.savefig("psi_theta_rel.png", dpi=250, bbox_inches="tight")

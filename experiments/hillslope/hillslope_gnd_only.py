from pathlib import Path

import matplotlib.pyplot as plt
from utils import RainDirichlet, get_hillslope_params, hillslope_transform

from coupling.ground import LinearProfile, standalone_ground
from postprocessing.plot_richards import animate, animate_line, line_plot_psi, load_data

plotting_dir = Path("plots/nonlinear_code")
plotting_dir.mkdir(parents=True, exist_ok=True)

params = get_hillslope_params(Lz=20, soil_type="loam")

initial_condition = LinearProfile(5.0, params)
boundary_condition = RainDirichlet(params)
standalone_ground(
    params,
    initial_condition,
    boundary_condition,
    hillslope_transform,
)


line_plot_psi(30, params)

psi, capacity, conductivity = load_data(params)

ani = animate_line(psi, params)
ani.save(filename=plotting_dir / "richards_psi_line.mp4", writer="ffmpeg")
plt.close("all")

ani = animate(psi, params)
ani.save(filename=plotting_dir / "richards_psi.mp4", writer="ffmpeg")
plt.close("all")


# theta = np.vectorize(theta_rel)(psi, silt_loam)
# ani = animate(theta, params, name=r"\theta")
# with warnings.catch_warnings(action="ignore"):
#     ani.save(filename="richards_theta.mp4", writer="ffmpeg")
# plt.close("all")

import dataclasses
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
from utils import analytical_solution, get_hillslope_params, rain_flux

from coupling.sfc_kinematic_wave import FlatSurface, OneSidedOutflow, standalone_surface
from postprocessing.plot_surface import load_surface

plotting_dir = Path("plots/nonlinear_code")
plotting_dir.mkdir(parents=True, exist_ok=True)

params = get_hillslope_params()
params = dataclasses.replace(
    params,
    t_end=18000,
    N=100,
    output_period=1,
    Mx=5,
    surface_slope=0.0005,
    mannings_coefficient=0.02,
)
initial_condition = FlatSurface(0.0)
boundary_condition = OneSidedOutflow(params)


v_ana = np.vectorize(lambda t: analytical_solution(t, params))
fig, ax = plt.subplots()
t = np.linspace(params.t_0, params.t_end, params.N)
ax.plot(t / 60, v_ana(t), color="k", ls="-", label="Analytical")


standalone_surface(params, initial_condition, boundary_condition, rain_flux)
h, v = load_surface(params)
outflow = h * v
t = np.linspace(params.t_0, params.t_end, int(params.N / params.output_period) + 1)
ax.plot(t / 60, outflow[:, 0], color="k", ls="--", label=r"Numerical, $\Delta x=$80m")

params = dataclasses.replace(
    params,
    Mx=400,
)
standalone_surface(params, initial_condition, boundary_condition, rain_flux)
h, v = load_surface(params)
outflow = h * v
t = np.linspace(params.t_0, params.t_end, int(params.N / params.output_period) + 1)
ax.plot(t / 60, outflow[:, 0], color="k", ls="-.", label=r"Numerical, $\Delta x=$1m")

ax.set(xlabel="Time $[min]$", ylabel="Outflow rate $[m^2 s^{-1}]$")
ax.legend()
fig.savefig(plotting_dir / "surface_analytical.pdf", bbox_inches="tight")

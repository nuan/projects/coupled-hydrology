import matplotlib.pyplot as plt
import meshio
import numpy as np
import ufl
from dune.ufl import Constant, DirichletBC
from utils import get_schneid_params

from coupling.ground import GroundDirichletBC, LinearProfile, standalone_ground
from coupling.params import Params

plt.style.use("stylesheet.mpl")


class DrainageTrench(GroundDirichletBC):
    def __init__(self, t_d, params: Params):
        self.t_d = Constant(t_d, name="t_d")
        self.water_table = Constant(1, name="water_table")
        self.Lx = params.Lx
        self.Lz = params.Lz

    def get_dirichlet_bc(self, space, t) -> list[DirichletBC]:
        x, z = ufl.SpatialCoordinate(space)
        bc1 = DirichletBC(
            space,
            ufl.conditional(t <= self.t_d, -2 + 2.2 * t / self.t_d, 0.2),
            ufl.conditional(x < 1, 1, 0)
            * ufl.conditional(z > (self.Lz.value - 1e-8), 1, 0),
        )
        bc2 = DirichletBC(
            space,
            self.water_table - z,
            ufl.conditional(x > (self.Lx.value - 1e-8), 1, 0)
            * ufl.conditional(z < self.water_table, z - self.water_table, 0),
        )
        return [bc1, bc2]


soil_type = "clay"

if soil_type == "loam":
    params = get_schneid_params(
        soil_type=soil_type,
        t_end=(3 / 16) * 86400,
        N=3,
        output_directory="output/schneid_loam",
    )
    t_d = (1 / 16) * 86400
elif soil_type == "clay":
    params = get_schneid_params(
        soil_type=soil_type,
        t_end=3.0 * 86400,
        N=9,
        output_directory="output/schneid_clay",
    )
    t_d = 1.0 * 86400
else:
    raise ValueError("Schneid Benchmark only supports soil types 'loam' and 'clay'!")


boundary_condition = DrainageTrench(t_d, params)
initial_condition = LinearProfile(2, params)

standalone_ground(params, initial_condition, boundary_condition)


def plot_psi(n: int, params: Params):
    mesh = meshio.read(params.output_directory / f"richards_{n:05}.vtu")
    psi = mesh.point_data["psi"]
    x = np.linspace(0, params.Lx.value, params.Mx + 1)
    z = np.linspace(0, params.Lz.value, params.Mz + 1)
    X, Z = np.meshgrid(x, z)
    psi = np.reshape(psi, (params.Mz + 1, params.Mx + 1))
    fig, ax = plt.subplots()
    fig.set(figwidth=5, figheight=7)
    im = ax.contourf(X, Z, psi, levels=np.linspace(-2, 1, 100), cmap="viridis_r")
    im2 = ax.contour(
        im,
        levels=np.linspace(-2, 1, 13),
        colors="k",
        linestyles="solid",
        linewidths=0.5,
    )
    ax.set(
        xlabel="x",
        ylabel=r"z",
        title=r"$\psi(x,z)$ at $t=3d$",
    )
    cbar = fig.colorbar(im, ticks=[-2, -1, 0, 1])
    cbar.add_lines(im2)
    plt.show()
    return fig


fig = plot_psi(params.N, params)
fig.savefig(
    "plots/nonlinear_code/psi_schneid_benchmark_clay.png", dpi=300, bbox_inches="tight"
)

import ufl
from dune.ufl import Constant, DirichletBC
from utils import get_schneid_params

from coupling.ground import GroundDirichletBC, LinearProfile, standalone_ground
from coupling.params import Params
from postprocessing.plot_richards import animate_line, load_data


class RisingSurfacePotential(GroundDirichletBC):
    def __init__(self, t_d, params: Params):
        self.t_d = Constant(t_d, name="t_d")
        self.water_table = Constant(1, name="water_table")
        self.Lx = params.Lx
        self.Lz = params.Lz

    def get_dirichlet_bc(self, space, t) -> list[DirichletBC]:
        z = ufl.SpatialCoordinate(space)[1]
        bc1 = DirichletBC(
            space,
            ufl.conditional(t <= self.t_d, -2 + 0.25 * t / self.t_d, -1.75),
            ufl.conditional(z > (self.Lz.value - 1e-8), 1, 0),
        )
        return [bc1]


soil_type = "sandy_loam_5"

if soil_type == "loam":
    params = get_schneid_params(soil_type=soil_type, t_end=(3 / 16) * 86400, N=3)
    t_d = (1 / 16) * 86400
elif soil_type == "sandy_loam_5":
    params = get_schneid_params(
        soil_type=soil_type, t_end=3.0 * 86400, N=800, output_period=20, Mz=32
    )
    t_d = 1.0 * 86400
else:
    raise ValueError("This soil type is not supported.")


boundary_condition = RisingSurfacePotential(t_d, params)
initial_condition = LinearProfile(2, params)

standalone_ground(params, initial_condition, boundary_condition)
psi = load_data(params)[0]
ani = animate_line(psi, params)
ani.save(f"plots/nonlinear_code/schneid_{soil_type}.mp4")

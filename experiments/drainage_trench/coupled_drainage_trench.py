import dataclasses
import itertools
from multiprocessing import Process
from pathlib import Path

import matplotlib.pyplot as plt
import meshio
import numpy as np
from utils import CouplingBenchmark, get_schneid_params, rain_flux

import coupling.ground as gnd
import coupling.sfc_shallow_water as sfc
from coupling.coupling_layer import GroundCouplingLayer, ShallowWaterCouplingLayer
from coupling.params import Params, render
from linear_model.analysis import compute_coupling_behavior
from postprocessing.plot_richards import load_data
from postprocessing.postprocess_convergence import (
    compute_convergence_factors,
    compute_mean_convergence_factor,
)

plt.style.use("stylesheet.mpl")
plotting_dir = Path("plots/nonlinear_code")
plotting_dir.mkdir(parents=True, exist_ok=True)


def plot_psi(n: int, params: Params, **kwargs):
    mesh = meshio.read(params.output_directory / f"richards_{n:05}.vtu")
    psi = mesh.point_data["psi"]
    x = np.linspace(0, params.Lx.value, params.Mx + 1)
    z = np.linspace(0, params.Lz.value, params.Mz + 1)
    X, Z = np.meshgrid(x, z)
    psi = np.reshape(psi, (params.Mz + 1, params.Mx + 1))
    fig, ax = plt.subplots()
    fig.set(figwidth=5, figheight=7)
    im = ax.contourf(X, Z, psi, vmin=-2, vmax=1, cmap="viridis_r")
    ax.set(
        xlabel="x",
        ylabel=r"z",
    )
    ax.set(**kwargs)
    fig.colorbar(im)
    plt.show()
    return fig


def influx(t, x):
    return rain_flux(t, x, 0.1 / 3600, 7200)


def run_coupled_small_scale(params: Params) -> None:
    render(params)
    coupling_layer = GroundCouplingLayer(params)
    initial_condition = gnd.LinearProfile(2, params)
    boundary_condition = CouplingBenchmark(params)
    groundwater_proc = Process(
        target=gnd.simulate_ground,
        args=[params, coupling_layer, initial_condition, boundary_condition],
    )
    coupling_layer = ShallowWaterCouplingLayer(params)
    initial_condition = sfc.FlatSurface(1e-6)
    boundary_condition = sfc.Outflow()
    surface_proc = Process(
        target=sfc.simulate_surface,
        args=[params, initial_condition, boundary_condition, coupling_layer, influx],
    )
    groundwater_proc.start()
    surface_proc.start()
    groundwater_proc.join()
    surface_proc.join()


soil_types = ["clay", "loam", "mixed"]
output_dir_base = Path("output/drainage_trench")
output_dir_base.mkdir(parents=True, exist_ok=True)
convergence_log = Path("precice-Surface-convergence.log")

params = get_schneid_params(
    soil_type=soil_types[0],
    t_end=10800,
    N=300,
    output_period=10,
    output_directory=output_dir_base,
)


def run_base_cases():
    for soil_type in soil_types:
        output_dir = output_dir_base / soil_type

        experiment_params = dataclasses.replace(
            params, soil_type=soil_type, output_directory=output_dir
        )

        run_coupled_small_scale(experiment_params)
        convergence_log.rename(output_dir / convergence_log)


def run_dz_dependence():
    Mz_values = np.array([4, 8, 16, 32, 64])
    for soil_type in soil_types:
        for Mz in Mz_values:
            output_dir = output_dir_base / f"{soil_type}_{Mz=}"
            output_dir.mkdir(parents=True, exist_ok=True)

            experiment_params = dataclasses.replace(
                params,
                soil_type=soil_type,
                output_directory=output_dir,
                output_period=0,
                Mz=Mz,
            )

            run_coupled_small_scale(experiment_params)
            convergence_log.rename(output_dir / convergence_log)


def run_dx_dependence():
    Mx_values = [5, 10, 20, 40, 80]
    for soil_type in soil_types:
        for Mx in Mx_values:
            output_dir = output_dir_base / f"{soil_type}_{Mx=}"
            output_dir.mkdir(parents=True, exist_ok=True)

            experiment_params = dataclasses.replace(
                params,
                soil_type=soil_type,
                output_directory=output_dir,
                output_period=0,
                Mx=Mx,
            )

            run_coupled_small_scale(experiment_params)
            convergence_log.rename(output_dir / convergence_log)


def run_dt_dependence():
    N_values = [150, 300, 600, 1200, 2400]
    for soil_type in soil_types:
        for N in N_values:
            output_dir = output_dir_base / f"{soil_type}_{N=}"
            output_dir.mkdir(parents=True, exist_ok=True)

            experiment_params = dataclasses.replace(
                params,
                soil_type=soil_type,
                output_directory=output_dir,
                output_period=0,
                N=N,
            )

            run_coupled_small_scale(experiment_params)
            convergence_log.rename(output_dir / convergence_log)


def plot_grid_dependence():
    Mz_values = [4, 8, 16, 32, 64]
    Mx_values = [5, 10, 20, 40, 80]
    N_values = [150, 300, 600, 1200, 2400]
    fig, axs = plt.subplots(ncols=3, layout="constrained", sharey=True)
    fig.set(figwidth=9, figheight=4)
    linestyles = itertools.cycle(["-", "--", ":"])

    ax = axs[0]
    for soil_type in soil_types:
        convergence_factors = []
        for N in N_values:
            output_dir = output_dir_base / f"{soil_type}_{N=}"
            convergence_factor = compute_mean_convergence_factor(
                output_dir / convergence_log
            )
            convergence_factors.append(convergence_factor)
        ax.plot(
            params.t_end / np.array(N_values),
            convergence_factors,
            ls=next(linestyles),
            marker="x",
            color="k",
            label=soil_type.capitalize(),
        )
    ax.plot(
        (params.t_end / np.array(N_values))[1:-1],
        2e-7 * (params.t_end / np.array(N_values))[1:-1],
        ls="--",
        color=(0, 0, 1),
    )
    ax.annotate(
        r"$\mathcal{O}(\Delta t)$",
        (0.4, 0.3),
        color=(0, 0, 1),
        xycoords=ax,
        rotation=25,
        fontsize="large",
    )
    ax.set(
        xscale="log",
        yscale="log",
        xlabel=r"$\Delta t$",
    )

    ax = axs[1]
    for soil_type in soil_types:
        convergence_factors = []
        for Mx in Mx_values:
            output_dir = output_dir_base / f"{soil_type}_{Mx=}"
            convergence_factor = compute_mean_convergence_factor(
                output_dir / convergence_log
            )
            convergence_factors.append(convergence_factor)
        ax.plot(
            params.Lx.value / np.array(Mx_values),
            convergence_factors,
            ls=next(linestyles),
            marker="x",
            color="k",
            label=soil_type.capitalize(),
        )
    ax.set(
        xscale="log",
        yscale="log",
        xlabel=r"$\Delta x$",
    )

    ax = axs[2]
    for soil_type in soil_types:
        convergence_factors = []
        for Mz in Mz_values:
            output_dir = output_dir_base / f"{soil_type}_{Mz=}"
            convergence_factor = compute_mean_convergence_factor(
                output_dir / convergence_log
            )
            convergence_factors.append(convergence_factor)
        ax.plot(
            params.Lz.value / np.array(Mz_values),
            convergence_factors,
            ls=next(linestyles),
            marker="x",
            color="k",
            label=soil_type.capitalize(),
        )
    ax.set(
        xscale="log",
        yscale="log",
        xlabel=r"$\Delta z$",
    )
    ax.plot(
        (params.Lz.value / np.array(Mz_values))[1:-1],
        7e-7 * 1 / (params.Lz.value / np.array(Mz_values))[1:-1],
        ls="--",
        color=(0, 0, 1),
    )
    ax.annotate(
        r"$\mathcal{O}(\Delta z^{-1})$",
        (0.4, 0.3),
        color=(0, 0, 1),
        xycoords=ax,
        rotation=-25,
        fontsize="large",
    )

    axs[0].legend()

    fig.supylabel("Mean Convergence Factor")
    fig.savefig(plotting_dir / "drainage_grid_dependence.pdf", bbox_inches="tight")


def psi_at_t_end():
    fig, axs = plt.subplots(
        ncols=len(soil_types), layout="constrained", sharex=True, sharey=True
    )
    fig.set(figwidth=7, figheight=5)
    n = int(params.N / params.output_period)

    for index, soil_type in enumerate(soil_types):
        ax = axs[index]
        output_dir = output_dir_base / soil_type
        mesh = meshio.read(output_dir / f"richards_{n:05}.vtu")
        psi = mesh.point_data["psi"]
        x = np.linspace(0, params.Lx.value, params.Mx + 1)
        z = np.linspace(0, params.Lz.value, params.Mz + 1)
        X, Z = np.meshgrid(x, z)
        psi = np.reshape(psi, (params.Mz + 1, params.Mx + 1))
        im = ax.contourf(X, Z, psi, vmin=-2, vmax=1, cmap="viridis_r")
        ax.set(title=soil_type.capitalize())
    fig.supxlabel("x")
    fig.supylabel("z")
    fig.colorbar(im, label=r"$\psi$")
    fig.savefig(plotting_dir / "drainage_psi_end.pdf")


def convergence_factor_over_time():
    fig, axs = plt.subplots(ncols=len(soil_types), layout="constrained", sharey=True)

    lines = []
    for index, soil_type in enumerate(soil_types):
        output_dir = output_dir_base / soil_type
        ax = axs[index]
        convergence_factors = compute_convergence_factors(output_dir / convergence_log)
        t = np.arange(len(convergence_factors)) * params.dt
        line = ax.scatter(t / 3600, convergence_factors, color="k", marker=".")
    lines.append(line)

    for index, soil_type in enumerate(soil_types):
        output_dir = output_dir_base / soil_type
        ax = axs[index]
        _, capacity, conductivity = load_data(
            dataclasses.replace(params, output_directory=output_dir)
        )
        mean_capacity = capacity.mean(axis=(1, 2))
        mean_conductivity = conductivity.mean(axis=(1, 2))
        S_mean = np.zeros_like(mean_capacity)
        for index in range(len(mean_capacity)):
            S_mean[index] = compute_coupling_behavior(
                mean_conductivity[index],
                mean_capacity[index],
                params.Lz.value,
                params.dt,
                params.Mz,
            )[1]
        t = np.linspace(
            params.t_0, params.t_end, int(params.N / params.output_period) + 1
        )

        line = ax.scatter(
            t[1:] / 3600,
            abs(S_mean)[1:],
            color="k",
            marker="+",
        )
        ax.set(
            title=soil_type.capitalize(),
            yscale="log",
            ylim=[1e-6, 1],
        )
    lines.append(line)

    axs[0].legend(
        lines,
        [r"observed $\mathrm{CR}_n$", r"expected $|S|$"],
    )
    fig.supxlabel("Time [h]")
    fig.supylabel("Convergence Factor")
    fig.set(figheight=4, figwidth=9)
    fig.savefig(plotting_dir / "drainage_convergence_factors.pdf", bbox_inches="tight")

def max_material_parameters():
    for soil_type in soil_types:
        output_dir = output_dir_base / soil_type
        _, capacity, conductivity = load_data(
            dataclasses.replace(params, output_directory=output_dir)
        )
        max_capacity = capacity.max()
        max_conductivity = conductivity.max()
        product = (capacity * conductivity)
        max_product = product.max()
        c_at_max_product = capacity.flatten()[product.argmax()]
        K_at_max_product = conductivity.flatten()[product.argmax()]
        max_local_S = compute_coupling_behavior(
                K_at_max_product,
                c_at_max_product,
                params.Lz.value,
                params.dt,
                params.Mz,
            )[1]

        mean_capacity = capacity.mean(axis=(1, 2))
        mean_conductivity = conductivity.mean(axis=(1, 2))
        mean_product = mean_capacity * mean_conductivity
        max_mean_product = mean_product.max()
        c_at_max_mean_product = mean_capacity[mean_product.argmax()]
        K_at_max_mean_product = mean_conductivity[mean_product.argmax()]
        max_mean_S = compute_coupling_behavior(
                K_at_max_mean_product,
                c_at_max_mean_product,
                params.Lz.value,
                params.dt,
                params.Mz,
            )[1]

        with open("material_drainage.log", mode="a") as out_file:
            out_file.write(soil_type + "\n")
            out_file.write(f"{float(max_capacity) = :.3g}\n")
            out_file.write(f"{float(max_conductivity) = :.3g}\n")
            out_file.write(f"{float(max_product) = :.3g}\n")
            out_file.write(f"{float(c_at_max_product) = :.3g}\n")
            out_file.write(f"{float(K_at_max_product) = :.3g}\n")
            out_file.write(f"{float(max_local_S) = :.3g}\n\n")
            out_file.write(f"{float(max_mean_product) = :.3g}\n")
            out_file.write(f"{float(c_at_max_mean_product) = :.3g}\n")
            out_file.write(f"{float(K_at_max_mean_product) = :.3g}\n")
            out_file.write(f"{float(max_mean_S) = :.3g}\n\n")

max_material_parameters()

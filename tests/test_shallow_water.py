import dataclasses

import numpy as np
import ufl

from coupling.params import Params
from coupling.sfc_shallow_water import (
    FlatSurface,
    GaussianBump,
    Outflow,
    ShallowWaterSurface,
    SurfaceInitialCondition,
    Wall,
)

cpl_tolerance = 1e-3
max_iterations = 15
min_iterations = 1
omega = 1
coupling_scheme = "serial-implicit"
soil_type = "loam"

Lx = 1.0
Lz = 1.0
t_0 = 0
t_end = 0.5

N = 100
Mx = 30
Mz = 30

precice_config_template = "coupling/precice-config.xml.j2"
precice_config = "coupling/precice-config.xml"

output_directory = "."
output_period = 0

params = Params(
    tolerance=cpl_tolerance,
    max_iterations=max_iterations,
    min_iterations=min_iterations,
    coupling_scheme=coupling_scheme,
    omega=omega,
    t_0=t_0,
    t_end=t_end,
    N=N,
    Mx=Mx,
    Mz=Mz,
    Lx=Lx,
    Lz=Lz,
    precice_config=precice_config,
    precice_config_template=precice_config_template,
    soil_type=soil_type,
    output_directory=output_directory,
    output_period=output_period,
)


class TwoShock(SurfaceInitialCondition):
    def __init__(self, params: Params) -> None:
        self.Lx = params.Lx
        self.u_left = 2.0
        self.u_right = 0.0
        self.h = 1.0
        # using https://github.com/tkent198/SW_riemann_problem/blob/master/SWRiem.pdf
        self.h_star = 1.34178
        self.u_star = 1.0

    def get_initial_condition(self, space):
        x = ufl.SpatialCoordinate(space)[0]
        two_shock = ufl.conditional(
            x < self.Lx / 2, self.h * self.u_left, self.h * self.u_right
        )
        return ufl.as_vector([self.h, two_shock])


def test_steady_state():
    initial_condition = FlatSurface(1.0)
    boundary_condition = Wall()
    surface = ShallowWaterSurface(params, boundary_condition, initial_condition)
    state_before = surface.state.as_numpy
    surface.step()
    state_after = surface.state.as_numpy
    assert abs(state_after - state_before).max() == 0


def test_two_shock():
    two_shock_params = dataclasses.replace(params, Lx=5.0, Mx=500, t_end=0.3)
    boundary_condition = Outflow()
    two_shock = TwoShock(two_shock_params)
    surface = ShallowWaterSurface(two_shock_params, boundary_condition, two_shock)
    while surface.time.value < two_shock_params.t_end:
        surface.step()
        surface.end_time_step()

    height = surface.state.as_numpy[::2]
    velocity = surface.state.as_numpy[1::2] / height

    middle_index = int(two_shock_params.Mx / 2) - 1
    tolerance = 1e-2
    assert abs(velocity[0] - two_shock.u_left) < tolerance
    assert abs(velocity[-1] - two_shock.u_right) < tolerance
    assert abs(height[0] - two_shock.h) < tolerance
    assert abs(height[-1] - two_shock.h) < tolerance
    assert abs(height[middle_index] - two_shock.h_star) < tolerance
    assert abs(velocity[middle_index] - two_shock.u_star) < tolerance


def test_convergence():
    convergence_params = dataclasses.replace(params, Lx=5.0, t_end=0.5, Mx=500)
    boundary_condition = Wall()
    initial_condition = GaussianBump(convergence_params)

    def time_loop():
        t = convergence_params.t_0
        while t < convergence_params.t_end:
            surface.step()
            surface.end_time_step()
            t += convergence_params.dt

    surface = ShallowWaterSurface(
        convergence_params, boundary_condition, initial_condition
    )
    time_loop()
    height_coarse = surface.state.as_numpy[::2]
    velocity_coarse = surface.state.as_numpy[1::2] / height_coarse

    convergence_params.Mx *= 2
    surface = ShallowWaterSurface(
        convergence_params, boundary_condition, initial_condition
    )
    time_loop()
    height_fine = surface.state.as_numpy[::4]
    velocity_fine = surface.state.as_numpy[1::4] / height_fine

    assert height_fine.shape == height_coarse.shape
    assert velocity_coarse.shape == velocity_fine.shape

    tolerance = 1e-2
    assert np.mean(abs(height_coarse - height_fine)) < tolerance
    assert np.mean(abs(velocity_coarse - velocity_fine)) < tolerance

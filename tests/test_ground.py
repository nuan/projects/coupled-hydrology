import numpy as np
from dune.ufl import DirichletBC

from coupling.ground import Ground, GroundDirichletBC, LinearProfile
from coupling.params import Params

cpl_tolerance = 1e-3
max_iterations = 15
min_iterations = 1
omega = 1
coupling_scheme = "serial-implicit"
soil_type = "loam"

Lx = 1.0
Lz = 1.0
t_0 = 0
t_end = 1000

N = 100
Mx = 30
Mz = 30

precice_config_template = "coupling/precice-config.xml.j2"
precice_config = "coupling/precice-config.xml"

output_directory = "."
output_period = 0

params = Params(
    tolerance=cpl_tolerance,
    max_iterations=max_iterations,
    min_iterations=min_iterations,
    coupling_scheme=coupling_scheme,
    omega=omega,
    t_0=t_0,
    t_end=t_end,
    N=N,
    Mx=Mx,
    Mz=Mz,
    Lx=Lx,
    Lz=Lz,
    precice_config=precice_config,
    precice_config_template=precice_config_template,
    soil_type=soil_type,
    output_directory=output_directory,
    output_period=output_period,
)


class HomogeneousDBC(GroundDirichletBC):
    def get_dirichlet_bc(self, space, t) -> list[DirichletBC]:
        homogeneous_dbc = DirichletBC(
            space,
            0.0,
        )
        return [homogeneous_dbc]


def test_convergence():
    def time_loop():
        t = params.t_0
        while t < params.t_end:
            ground.step()
            ground.end_time_step()
            t += params.dt

    initial_condition = LinearProfile(1, params)
    boundary_condition = HomogeneousDBC()
    ground = Ground(params, initial_condition, boundary_condition)
    time_loop()
    psi_coarse = np.reshape(ground.psi_h.as_numpy, (params.Mx + 1, params.Mz + 1))

    params.Mx *= 2
    params.Mz *= 2
    ground = Ground(params, initial_condition, boundary_condition)
    time_loop()
    psi_fine = np.reshape(ground.psi_h.as_numpy, (params.Mx + 1, params.Mz + 1))
    psi_fine = psi_fine[::2, ::2]

    assert psi_coarse.shape == psi_fine.shape

    tolerance = 1e-3
    assert np.mean(abs(psi_coarse - psi_fine)) < tolerance
